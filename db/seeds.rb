User.create!(
	name: 'Will Smith',
	email: 'admin@example.com', 
	password: 'password', 
	password_confirmation: 'password', 
	is_admin: true
) 

User.create!(
	name: 'John Doe',
	email: 'user@example.com', 
	password: 'password', 
	password_confirmation: 'password', 
) 

Category.create!(
	title: 'Restaurants'
	) 

Category.create!(
	title: 'Cafes'
	)

Category.create!(
	title: 'Pubs'
	)

Category.create!(
	title: 'Taverns'
	)

5.times do 
	User.create!(
		name: Faker::Name.unique.name,
		email: Faker::Internet.email, 
		password: 'password', 
		password_confirmation: 'password'
	)
end

User.all.each do |user|
	3.times do
	  for i in 1..5
	    place = user.places.build(title: Faker::Restaurant.name, desc: Faker::Restaurant.description, category_id: "#{rand(1..4)}", agreement: true)
	    path = Rails.root.join('app', 'assets', 'images', 'seed', "#{i}.jpeg")
	    place.image.attach(io: File.open(path), filename: "#{i}.jpeg")
	    place.save
	  end
	end
end

Place.all.each do |place|
	5.times do
    review = place.reviews.build(
		body: Faker::Lorem.sentence,
		quality: rand(1..5), 
		service: rand(1..5),
		interior: rand(1..5),
		user_id: rand(1..5)
	)
  review.save
  end
end

Place.all.each do |place|
  for i in 1..5
    gallery = place.galleries.build(user_id: "#{rand(1..5)}")
    path = Rails.root.join('app', 'assets', 'images', 'seed', "#{i}.jpeg")
    gallery.image.attach(io: File.open(path), filename: "#{i}.jpeg")
    gallery.save
  end
end