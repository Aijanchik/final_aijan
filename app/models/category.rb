class Category < ApplicationRecord
	has_many :places
	validates :title, presence: true, 
			  length: { maximum: 30 }
end
