class Place < ApplicationRecord
	paginates_per 20
  belongs_to :user
  belongs_to :category
  has_many :reviews, :dependent => :destroy
  has_many :galleries, :dependent => :destroy
  has_one_attached :image
  validates :title, presence: true, 
			  length: { maximum: 30 }
	validates :desc, presence: true, 
			  length: { maximum: 300 }
	validates :agreement, inclusion: { in: [true] }
	validate :image_type

	def self.search_by(search_term)
		where("LOWER(title) LIKE :search_term",
		search_term: "%#{search_term.downcase}%")
	end

	private

	def image_type
		if image.attached? == false
			errors.add(:image, "is missing")
		elsif !image.content_type.in?(%("image/jpeg image/png"))
				errors.add(:image, "needs to be JPEG or PNG or JPG")
		end
	end
end


