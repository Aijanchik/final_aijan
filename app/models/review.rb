class Review < ApplicationRecord
	paginates_per 5
  belongs_to :user
  belongs_to :place

  validates :body, length: { maximum: 300 }
  validates :quality, presence: true, 
			  numericality: { only_integer: true, 
			  				  greater_than_or_equal_to: 1, 
			  				  less_than_or_equal_to: 5 }
	validates :service, presence: true, 
			  numericality: { only_integer: true, 
			  				  greater_than_or_equal_to: 1, 
			  				  less_than_or_equal_to: 5 }
	validates :interior, presence: true, 
			  numericality: { only_integer: true, 
			  				  greater_than_or_equal_to: 1, 
			  				  less_than_or_equal_to: 5 }

	def average_raitings 
		average_raitings =	(Review.average(:quality) + Review.average(:service) + Review.average(:interior))/3
		sprintf("%.2f", average_raitings) 
	end

	def average_quality
		quality = Review.average(:quality)
		sprintf("%.2f", quality) 
	end

	def average_service 
		service= Review.average(:service)
		sprintf("%.2f", service) 
	end

	def average_interior 
		interior = Review.average(:interior)
		sprintf("%.2f", interior)
	end
end
