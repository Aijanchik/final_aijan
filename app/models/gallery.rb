class Gallery < ApplicationRecord
  belongs_to :place
  belongs_to :user
  has_one_attached :image

  validate :image_type

	private

	def image_type
		if image.attached? == false
			errors.add(:image, "is missing")
		elsif !image.content_type.in?(%("image/jpeg image/png"))
				errors.add(:image, "needs to be JPEG or PNG or JPG")
		end
	end
end
