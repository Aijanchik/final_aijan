class ReviewsController < ApplicationController
  def index
    @reviews = Review.order(:created_at).page params[:page]
  end


  def create
    @place = Place.find(params[:place_id])
    @review = current_user.reviews.build(review_params)
		@review.place = @place
		if @review.save
      flash[:success] = "Review is successfully added!"
    else
      flash[:warning] = "Incorrect!"
    end
    redirect_to @place
  end

  private

  def review_params
    params.require(:review).permit(:body, :quality, :service, :interior)
  end


end
