class GalleriesController < ApplicationController
  def create
    @place = Place.find(params[:place_id])
    @gallery = current_user.galleries.build(gallery_params)
		@gallery.place = @place
		if @gallery.save
      flash[:success] = "Review is successfully added!"
    else
      flash[:warning] = "Incorrect!"
    end
    redirect_to @place
  end

 

  private

 

  def gallery_params

    params.require(:gallery).permit(:image)

  end
end
