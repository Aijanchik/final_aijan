class PlacesController < ApplicationController
  load_and_authorize_resource

  before_action :set_place, only: [:edit, :update, :destroy]

  def index
    @places = Place.order(:title).page params[:page]
    @categories = Category.all
  if params[:search]
    @search_term = params[:search]
    @places = @places.search_by(@search_term)
  end
  end

  def new
    @place = Place.new
  end

  def show
    @place = Place.find(params[:id])
    @review = Review.new
    @gallery = Gallery.new
    @reviews = @place.reviews.order(:created_at).page params[:page]
  end

  def create
    @place = current_user.places.build(place_params)
    if @place.save
      flash[:success] = "Place is successfully added!"
      redirect_to @place
    else
      flash[:warning] = "Incorrect!"
      render 'edit'
    end
  end

  def edit
  end

  def update
    @place.update(place_params)
    if @place.save
      flash[:success] = "Place is successfully updated!"
      redirect_to @place
    else
      flash[:warning] = "Incorrect!"
      render 'edit'
    end
  end

  def destroy
    @place.destroy
    flash[:success] = "Place is successfully deleted"
    redirect_to root_path
  end
  

  private
  def place_params
    params.require(:place).permit(:title, :desc, :agreement, :category_id, :user_id, :image)
  end

  def set_place
    @place = current_user.places.find(params[:id])
  end
end


