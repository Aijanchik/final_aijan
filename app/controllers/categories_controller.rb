class CategoriesController < ApplicationController
	before_action :set_category, only: [:show]
  def index
  	@categories = Category.all
  end

  def show
    @categories = Category.all
  end

  private

  def set_category
  	@category = Category.find(params[:id])
  end

  def genre_params
  	params.require(:category).permit(:title)
  end
end

