ActiveAdmin.register Place do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :title, :desc, :agreement, :user_id, :category_id, :image
  #
  # or
  #
  # permit_params do
  #   permitted = [:title, :desc, :agreement, :user_id, :category_id]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end


  index do
    selectable_column
    id_column
    column :image do |place|
      image_tag place.image.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
    end
    column :title do |place|
      link_to place.title, admin_place_path(place)
    end
    column :desc
    column :agreement
    column :category
    column :user
    actions
  end

end
